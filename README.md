# Isospin Equilibration in Neutron Star Mergers

In this repo you can find all necessary data files and the corresponding jupyter notebook (which can be run in google colab) to create the plots in the paper "Isospin Equilibration in Neutron Star Mergers" by M.G. Alford, A. Haber and Z. Zhang arxiv:XXXX.XXXX .

If you have any questions contact Alexander Haber, ahaber@physics.wustl.edu